import os
import logging

from influxdb import InfluxDBClient

influxdb = InfluxDBClient(os.getenv('INFLUXDB_HOST', 'localhost'),
                          int(os.getenv('INFLUXDB_PORT', '8086')),
                          os.getenv('INFLUXDB_USER', 'root'),
                          os.getenv('INFLUXDB_PASS', 'root'),
                          os.getenv('INFLUXDB_DATABASE', 'test'))

from plaza_bridge import (
    PlazaBridge,  # Import bridge functionality
    CallbackBlockArgument,  # Needed for argument definition
    BlockArgument,
)

# Initialization
bridge = PlazaBridge(
    name="InfluxDB",
    endpoint=os.environ['BRIDGE_ENDPOINT'],
    is_public=False,
)

# Bridge actions
@bridge.callback("get_databases")
def get_databases(_extra_data):
    logging.info('DATABASES')
    dbs = influxdb.get_list_database()
    return [
        {
            'id': db['name'],
            'name': db['name'],
        }
        for db in dbs
        if db['name'] != '_internal'
    ]

@bridge.operation(
    id="save_metric",
    message="Save metric %1 as %2",
    arguments=[
        BlockArgument(int, 1),
        BlockArgument(str, "metric name"),
    ],
)
def save_metric(metric, metric_name, extra_data):
    logging.info('SAVE {}'.format(metric_name))
    influxdb.write_points([{
        "measurement": metric_name,
        "fields": {"value": metric},
    }])
    return metric

# Run
if __name__ == '__main__':
   logging.basicConfig(format="%(asctime)s - %(levelname)s [%(filename)s] %(message)s")
   logging.getLogger().setLevel(logging.INFO)

   logging.info('Starting bridge')
   bridge.run()
